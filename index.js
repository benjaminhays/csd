// AWS Iot Button - Text Message Allert
// Ben Hays
// February 2018

'use strict';

const AWS = require('aws-sdk');

const SNS = new AWS.SNS({ apiVersion: '2010-03-31' });
const ADULTCHILD = '15551234567'; // Replace with your number

exports.handler = (event, context, callback) => {
    console.log('Received event:', event);


    if (event.clickType == "LONG")
    {
	const payload = JSON.stringify(event);
	const params = {
	    PhoneNumber: ADULTCHILD,
	    Message: "HELP! Ben is too awesome!", // Replace with long press message
	};
	SNS.publish(params, callback);
    }

    if (event.clickType == "SINGLE")
    {
	const payload = JSON.stringify(event);
	const params = {
	    PhoneNumber: ADULTCHILD,
	    Message: "IoT Button Works.", // Replace with single press message
	};
	SNS.publish(params, callback);
    }

    if (event.clickType == "DOUBLE")
    {
	const payload = JSON.stringify(event);
	const params = {
	    PhoneNumber: ADULTCHILD,
	    Message: "Ouch!", // Replace with double press message
	};
	SNS.publish(params, callback);
    }

    // result will go to function callback

};
